package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"launchpad.net/goyaml"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

func version() {
	os.Stderr.WriteString(`spyact: v0.3.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: spyact [OPTION] CONFIG_FILE

Options:
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

var waitTime = 1 * time.Second

func getModTime(name string) (modTime time.Time, err error) {
	info, err := os.Stat(name)
	if err != nil {
		return modTime, err
	}
	return info.ModTime(), nil
}

type Config struct {
	File    string
	Action  []string
	ModTime time.Time
}

func (conf *Config) FixModTime() (err error) {
	conf.ModTime, err = getModTime(conf.File)
	if err != nil {
		return err
	}
	return nil
}

func (conf *Config) IsModified() (b bool, err error) {
	modtime, err := getModTime(conf.File)
	if err != nil {
		return false, err
	}
	return !modtime.Equal(conf.ModTime), nil
}

func (conf *Config) ExecAction() {
	if 0 < len(conf.Action) {
		action := exec.Command(conf.Action[0], conf.Action[1:]...)
		action.Run()
	}
}

type ConfigList []Config

func (confList *ConfigList) LoadConfig(confFile string) (err error) {
	yaml, err := ioutil.ReadFile(confFile)
	if err != nil {
		return err
	}
	return goyaml.Unmarshal(yaml, confList)
}

func (confList *ConfigList) Init() (err error) {
	ls := []Config(*confList)
	for i, conf := range ls {
		ls[i].File, err = filepath.Abs(conf.File)
		if err != nil {
			return err
		}
		ls[i].ModTime, err = getModTime(conf.File)
		if err != nil {
			return err
		}
	}
	return nil
}

func (confList *ConfigList) Monitoring() (err error) {
	ls := []Config(*confList)
	for i, conf := range ls {
		isModified, err := conf.IsModified()
		if err != nil {
			log.Println("ERRO:", conf.File)
			return err
		}

		if isModified {
			if err := ls[i].FixModTime(); err != nil {
				log.Printf("ERRO:", ls[i].File)
				return err
			}
			log.Println("INFO:", "modified", ls[i].File)
			go ls[i].ExecAction()
		}
	}
	return nil
}

func _main() (exitCode int, err error) {
	var (
		isHelp    = flag.Bool("help", false, "show this help message")
		isVersion = flag.Bool("version", false, "print the version")
	)
	flag.Usage = usage
	flag.Parse()

	if *isHelp {
		usage()
		return 0, nil
	}
	if *isVersion {
		version()
		return 0, nil
	}

	var confList ConfigList
	if flag.NArg() < 1 {
		return 1, fmt.Errorf("no config file")
	}
	confFile := flag.Arg(0)
	err = confList.LoadConfig(confFile)
	if err != nil {
		return 1, err
	}
	confList.Init()

	log.Println("INFO:", "Started monitoring")
	defer log.Println("INFO:", "Finished monitoring")
	for {
		err := confList.Monitoring()
		if err != nil {
			log.Println("ERRO:", err)
			return 1, err
		}
		time.Sleep(waitTime)
	}
	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "spyact:", err)
	}
	os.Exit(exitCode)
}
