spyact
====================
monitoring modified file and action, using yaml format config

License
====================
MIT License

Usage
====================
	$ spyact CONFIG_FILE

	Options:
		--help       show this help message
		--version    print the version

Example
====================
	$ spyact ~/.config.yml
	starting monitoring with

Config
====================
Describe config with yaml formatted file

###Example(config.yml)
	- file: C:\Vim\vimrc
	  action: ['shutdown', '/f', '/s', '/t', '0']
	- file: C:\Users\kusabashira\.cache\
	  action: ['rm', '-rf', 'C:\Users\kusabashira\.cache']

If modified `C:/Vim/vimrc`,
Exec `shutdown /f /s /t 0`.

If modified `C:/Users/kusabashira/.cache/`,
Exec `rm -rf C:/Users/kusabashira/.cache/`.

Author
====================
Kusabashira <kusabashira227@gmail.com>
